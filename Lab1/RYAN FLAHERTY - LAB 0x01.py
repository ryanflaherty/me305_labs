''' @file      FSM_template.py
    @brief     Finite State Machine Skeleton Code
    @details   This file will be used to demonstrate one method for
               implementing a finite state machine in Python.
'''

# Import statements go at top near docstring
import time

# Functions should be defined before the main part of the program


# The main program should go after function definitions and run continuously
if __name__ == '__main__':
    ## The current state for this iteration of the FSM
    state = 0
    ## The number of iterations performed by the FSM
    runs = 0
    
    input('Hello, this is a program which cycles through 3 LED patterns.'
          'The way to change which LED pattern is displayed is to press'
          'the user button (B1 - The Blue Button) on the Nucleo.'
          
          'Press "Enter" to continue')
          
    while(True):
        try:
            if(state==0):
                # Run state 0 code
                print('Running State 0')
                state = 1                   # Transition to state 1
            
            elif(state==1):
                # Run state 1 code
                print('Running State 1')
                state = 2                   # Transition to state 2
            
            elif(state==2):
                # Run state 2 code
                print('Running State 2')
                state = 3                   # Transition to state 3
            
            elif(state==3):
                # Run state 3 code
                print('Running State 3')
                state = 4                   # Transition to state 4
            
            elif(state==4):
                # Run state 4 code
                print('Running State 4')
                state = 2                   # Transition to state 2
                
            time.sleep(1)                   # Slow down the FSM so we can
                                            # actually read the console output
                                    
            runs += 1                       
                                    
        except KeyboardInterrupt:           # If keyboard interrupt, exit loop
            break
        
    print('Program Terminating')