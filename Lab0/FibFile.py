# -*- coding: utf-8 -*-
"""
@file FibFile.py
@author: Ryan Flaherty
"""


'''
@brief This function calculates a Fibonacci number at a specific index.
@param idx An integer specifying the index of the desired Fibonacci number
'''

# The line below creates an empty array which will hold 
# the Fibonacci sequence values
my_fib_array = {}

#Defines a function to hold the Fibonacci sequence code
def my_fib(idx):

    # If we have stored the value, then return it
    if idx in my_fib_array:
        return my_fib_array[idx]
    # defines the first two idx values as 0 and 1 and 
    # creates a formula to calculate values past the 
    # first two terms of the sequence
    elif idx == 0:
        value = 0
    elif idx == 1:
        value = 1
    elif idx > 1:
        # Calculates the Fibonacci sequence term 
        # corresponding to a specific index and 
        # assigns it to the variable "value"
        value = my_fib(idx-1) + my_fib(idx-2)
    
    # Defines the Fibonacci sequence term at the specified idx
    # and puts it in the empty array that was created to hold 
    # the Fibonacci sequence values
    my_fib_array[idx] = value
    return value

if __name__ == '__main__':
    
    # Begins an infinite while loop
    while True:
        # Asks the user to input an index value which will be used to 
        # return the Fibonacci sequence term that occurs at that 
        # specific index position or to quit the program
        idx = input('Enter Fibonacci Index (or enter "q" to quit program): ')
        
        # if the user inputs 'q' break the infinite while loop
        if idx == "q":
            break
        
        # Else convert the users input into an integer and
        # return the fibonacci sequence value at that user specified index
        elif idx != "q":
            idx = int(idx)
            # Returns the calculated Fibonacci value in a user friendly 
            # and easy to comprehend manner
        print ('Fibonacci number at '
               'index {:} is {:}.'.format(idx,my_fib(idx)))
