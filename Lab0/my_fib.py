# -*- coding: utf-8 -*-
'''
@file my_fib.py
@author: Ryan Flaherty
'''
def my_fib(idx):
    '''
    @brief     This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired
               Fibonacci number
    '''
    idx = int(input('Enter Fibonacci Index (must be positive integer): '))

    a = 0
    b = 1
    num_sum = 0
    count = 1
    print('Fibonacci Series: ' , end = ' ')
    while(count <= idx):
        print(num_sum , end = ' ')
        count = count + 1
        a = b
        b = num_sum
        num_sum = a + b
        
if __name__ == '__main__':

    """
    print ('Fibonacci number at '
           'index {:} is {:}.'.format(idx,my_fib(idx)))
    """